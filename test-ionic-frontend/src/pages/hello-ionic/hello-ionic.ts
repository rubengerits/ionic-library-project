import {Component} from '@angular/core';
import {SampleService} from 'test-ionic-backend';


@Component({
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  constructor(private sampleService: SampleService) {
    this.sampleService.logMessage("test");
  }
}
