import {Logger} from 'test-ionic-backend';

export class MyLogger extends Logger {

  info(message: string): void {
    console.info(message);
  }

}
