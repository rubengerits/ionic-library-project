import {Logger} from './logger';
import {Inject} from '@angular/core';

export class SampleService {

    private logger: Logger;

    constructor(@Inject(Logger) logger: Logger) {
        this.logger = logger;
    }

    public logMessage(message: string): void {
        this.logger.info(message);
    }
}
