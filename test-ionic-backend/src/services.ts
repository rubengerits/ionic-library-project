// Import all services
import {SampleService} from './services/sample.service';
import {Logger} from './services/logger';

// Export all services
export * from './services/sample.service';
export * from './services/logger';

// Export convenience property
export const PROVIDERS: any[] = [
    Logger,
    SampleService
];
